## text module ##
text_ui <- function(id) {
  ns <- NS(id) # Note the NS() function - namespaces are the most important thing in a module!
  fluidRow(
    textOutput(outputId = ns("text")) 
  ) # https://mastering-shiny.org/scaling-modules.html
}   # Namespace is called a namespace because it creates “spaces” of “names” (ids)
    # that are isolated from the rest of the app. 

textServer <- function(id, df, var, threshhold) { # var is a metric of interest
  moduleServer(id, function(input, output, session) {
  n <- reactive({ 
         sum(df()[[var]] > threshhold) 
       })
  output$text <- renderText({
    paste("In this month", var, 
          "exceeded the average daily threshhold of", threshhold,
          "a total of", n(), "days")
    })
  })
}

# # Uncomment and run to test the module separately:
# text_demo <- function() {
#   df <- data.frame(day = 1:30, arr_delay = 1:30) # text_demo() is using fake data
#   ui <- fluidPage(text_ui("x"))
#   server <- function(input, output, session) {
#     textServer("x", reactive({df}), "arr_delay", 15)
#   }
#   shinyApp(ui, server)
# }
# 
# text_demo()
