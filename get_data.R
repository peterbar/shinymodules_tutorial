ua_data <-
  nycflights13::flights %>%
  filter(carrier == "UA") %>%
  mutate(ind_arr_delay = (arr_delay > 5)) %>%
  group_by(year, month, day) %>%
  summarize(n = n(), # n() gives the current group size
            across(ends_with("delay"), mean, na.rm = TRUE)) %>%
  ungroup()

# # To take a look at the data, uncomment and run:
# head(ua_data)