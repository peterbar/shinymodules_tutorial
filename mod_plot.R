# plot module ----
plot_ui <- function(id) {
  # ns <- NS(id)
  fluidRow(
    column(10, plotOutput(outputId = NS(id, "plot"))), # Note this is another way to call NS()
    column(2, downloadButton(outputId = NS(id, "dnld"), label = "Download"))
  )
}

plotServer <- function(id, df, var, threshhold = NULL) { # var is a metric of interest
  moduleServer(id, function(input, output, session) {
    plot <- reactive({
      viz_monthly(df(), var, threshhold) # viz_monthly() defined in 'viz_monthly.R', made reactive here
    }) 
    output$plot <- renderPlot({plot()})  # plot() is reactive
    output$dnld <- downloadHandler(
      filename = function() {paste0(var, '.png')},
      content = function(file) {ggsave(file, plot())}
    )
  })
}

# # Uncomment and run to test the module separately:
# plot_demo <- function() {
#   df <- data.frame(day = 1:30, arr_delay = 1:30) # plot_demo() is using fake data
#   ui <- fluidPage(plot_ui("x"))
#   server <- function(input, output, session) {
#     plotServer("x", reactive({df}), "arr_delay")
#   }
#   shinyApp(ui, server)
# }
# 
# plot_demo()